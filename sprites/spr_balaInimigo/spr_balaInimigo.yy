{
    "id": "1b35415c-fe80-4b98-88fe-db09d7635f49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_balaInimigo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c3432eb-b34e-4d0b-b6b0-b94414726daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b35415c-fe80-4b98-88fe-db09d7635f49",
            "compositeImage": {
                "id": "50cc45d0-d3ea-4962-a854-4a7b739c8564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3432eb-b34e-4d0b-b6b0-b94414726daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "023c769a-1cfa-445e-bf75-ce7a937996c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3432eb-b34e-4d0b-b6b0-b94414726daf",
                    "LayerId": "634ed7c0-c0ec-456d-a49a-6fbb81f54fd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "634ed7c0-c0ec-456d-a49a-6fbb81f54fd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b35415c-fe80-4b98-88fe-db09d7635f49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}