{
    "id": "07a4a626-c172-4c12-b670-a06c6384d78d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6c2fecb-8db3-4b69-a116-430e18028e64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a4a626-c172-4c12-b670-a06c6384d78d",
            "compositeImage": {
                "id": "ec58865d-2cd3-4238-bffd-e8c2771913c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c2fecb-8db3-4b69-a116-430e18028e64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03a88234-e970-423c-be1f-4106bcb9629f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c2fecb-8db3-4b69-a116-430e18028e64",
                    "LayerId": "67987946-2b71-4e74-b3ec-a84d6684362d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "67987946-2b71-4e74-b3ec-a84d6684362d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a4a626-c172-4c12-b670-a06c6384d78d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}