{
    "id": "43b07ee5-4e77-459f-a813-7ec6234182a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2b76e34-d7d1-479a-a01f-f810b43dca26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43b07ee5-4e77-459f-a813-7ec6234182a4",
            "compositeImage": {
                "id": "77772607-6777-4722-b935-4b38b09abbf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b76e34-d7d1-479a-a01f-f810b43dca26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804a84c5-9747-4da3-a9d9-f40a0e7fb2a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b76e34-d7d1-479a-a01f-f810b43dca26",
                    "LayerId": "2b377267-1331-46b8-995b-10320098ac18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2b377267-1331-46b8-995b-10320098ac18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43b07ee5-4e77-459f-a813-7ec6234182a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}