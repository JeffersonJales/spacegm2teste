{
    "id": "f1876341-8145-442b-aad7-e003f69406b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe1d2696-bdc7-42b4-9908-e23e2b91a49c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1876341-8145-442b-aad7-e003f69406b1",
            "compositeImage": {
                "id": "88ad9d45-ad6f-4c84-a9d4-b7f52be1c6b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe1d2696-bdc7-42b4-9908-e23e2b91a49c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d15a0ba6-24a1-487c-9395-0820bd0b2226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe1d2696-bdc7-42b4-9908-e23e2b91a49c",
                    "LayerId": "4da322c8-410d-4197-90f6-3caede4d1c72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4da322c8-410d-4197-90f6-3caede4d1c72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1876341-8145-442b-aad7-e003f69406b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}