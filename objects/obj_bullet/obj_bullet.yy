{
    "id": "e9a2250e-999b-49af-8acc-6081ed66e903",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "5807f40f-cc33-408b-9e33-347b3c3ff4b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "e9a2250e-999b-49af-8acc-6081ed66e903"
        },
        {
            "id": "c8bd9010-5f98-47d5-8fcc-6d078889f673",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6e52f44a-8f40-4630-a748-af73f5a9a995",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9a2250e-999b-49af-8acc-6081ed66e903"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "07a4a626-c172-4c12-b670-a06c6384d78d",
    "visible": true
}