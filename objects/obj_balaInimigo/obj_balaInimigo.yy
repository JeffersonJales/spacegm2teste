{
    "id": "b7d18fe9-cb7a-43c8-993c-5b1ce0ed1ba6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_balaInimigo",
    "eventList": [
        {
            "id": "d3c549b9-940e-49cf-9991-3faaebd30ebc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "b7d18fe9-cb7a-43c8-993c-5b1ce0ed1ba6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "1b35415c-fe80-4b98-88fe-db09d7635f49",
    "visible": true
}