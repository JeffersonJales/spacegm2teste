/// @description Atirar Bala
if(atirarSozinho || !podeAtirar)
	return;

var newBullet = instance_create_depth(x,y - sprite_height/2, 0, obj_bullet);
newBullet.vspeed = velocidadeBala;
podeAtirar = false;
alarm[1] = tempoParaAtirar;