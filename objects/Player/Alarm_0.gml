/// @description Atirar automático
if (!atirarSozinho)
	return;

var newBullet = instance_create_depth(x,y - sprite_height/2, 0, obj_bullet);
newBullet.vspeed = velocidadeBala;
alarm[0] = tempoParaAtirar;