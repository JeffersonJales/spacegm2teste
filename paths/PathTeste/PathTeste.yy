{
    "id": "bff1d44c-83ca-460d-8885-b9370a6f4217",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "PathTeste",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "ac673df8-9949-44a4-ae69-342f4c965536",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": -64,
            "speed": 100
        },
        {
            "id": "8cabc1c8-78bb-4ed1-9aab-bf3d5a5cacf6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 389,
            "y": 100,
            "speed": 200
        },
        {
            "id": "fd5f3bba-2077-45e5-ae44-ef4f28ee168f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 236,
            "speed": 400
        },
        {
            "id": "b3e3a51b-b272-44e6-aab8-c6ce0a50b4a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 156,
            "y": 235,
            "speed": 200
        },
        {
            "id": "1d676df2-14c9-44be-9828-617ae8390953",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 161,
            "y": 80,
            "speed": 100
        },
        {
            "id": "cdf0c9c5-3da6-41ed-9635-4a81ffb8e8cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": -64,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}